﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SL {
	public static class Encryption {

		public static string Make(String pStr) {
			MD5CryptoServiceProvider Md5 = new MD5CryptoServiceProvider();
			Byte[] data;
			Byte[] resultado;
			data = Encoding.Unicode.GetBytes(pStr);
			resultado = Md5.ComputeHash(data);
			StringBuilder tempStr = new StringBuilder(resultado.Length * 2);
			foreach (byte b in resultado) {
				tempStr.AppendFormat("{0:x2}", b);
			}
			String Encrypted = tempStr.ToString();
			return tempStr.ToString();
		}

	}
}
