﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace SL {
	public class Backup {
		public static bool realizarBackUp(string path, string userName) {
			try {
				BackupDAO dao = new BackupDAO();
				dao.backUp(path);
				Bitacora.RegistrarBackUp(userName);
				return true;
			} catch (Exception e) {
				return false;
			}
		}

		public static bool realizarRestore(string path, string userName) {
			try { 
				BackupDAO dao = new BackupDAO();
				dao.restore(path);
				Bitacora.RegistrarRestore(userName);
				return true;
			} catch (Exception e) {
				return false;
			}
}
	}
}
