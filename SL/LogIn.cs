﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SL {
	public static class LogIn {
		public static DTO.UsuarioDTO Ingresar(String pUser, String pPass) {
			DAL.UsuariosDAO mUDAO = new DAL.UsuariosDAO();
			DTO.UsuarioDTO User = mUDAO.GetUsuario(pUser);

			if (User == null) {
				return null;
			}

			if (Encryption.Make(pPass) == User.pass) {
				mUDAO.CambiarNumeroIntentos(pUser, 0);
				return User;
			}

			if (User.intentos < 2) {
				mUDAO.CambiarNumeroIntentos(pUser, User.intentos + 1);
			} else {
				mUDAO.CambiarEstado(pUser);
				SL.Bitacora.RegistrarUsuarioBloqueado(pUser);
			}
			return null;
		}
	}
}
