﻿using DTO;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SL {
	public static class Bitacora {
		private static string[] messageType = new string[] {"info", "error", "warning"};

		public static List<MensajeDTO> GetMensajes() {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			return mBitDAO.GetMensajes();
		}

		public static void RegistrarLogin(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "El usuario ingresó al sistema.", messageType[0]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarLogout(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "El usuario salió del sistema.", messageType[0]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarFailedLogin(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "El usuario intento ingresar al sistema.", messageType[1]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarIngresoBitacoraUrl(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "Se ha intentado ingresar a la bitácora por la URL.", messageType[1]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarUsuarioBloqueado(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "El usuario ha sido bloqueado", messageType[2]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarBackUp(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "Se ha realizado un backup", messageType[2]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarRestore(string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "Se ha realizado un restore", messageType[2]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarErrorDVH(string fila, string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "Error DVH en fila: " + fila, messageType[2]);
			mBitDAO.Registrar(nuevo);
		}

		public static void RegistrarErrorDVV(string tabla, string pUsuario) {
			BitacoraDAO mBitDAO = new BitacoraDAO();
			MensajeDTO nuevo = new MensajeDTO(DateTime.Now, pUsuario, "Error DVV en tabla: " + tabla, messageType[2]);
			mBitDAO.Registrar(nuevo);
		}
	}
}
