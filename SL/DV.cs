﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SL {
	public class DV {
		static string[] tablas = new string[] { "Usuarios", "Permisos" };
		static List<string> errores = new List<string>();

		public static bool verificarDVH(string tabla, string userName) {
			List<string> rows = DVDAO.verificarDVH(tabla);

			if (rows.Count == 0) {
				return false;
			}

			foreach (string row in rows) {
				string errorMsg = $"Tabla: {tabla}, Error DVH en la fila: {row}";
				errores.Add(errorMsg);
				Bitacora.RegistrarErrorDVH(row, userName);
			}

            return true;
		}

		public static bool verificarDV(string userName) {
			bool ok = true;

			foreach (string tabla in tablas) {
				if (!DVDAO.verificarDVV(tabla)) {
					ok = false;
					string errorMsg = "Error DVV en la tabla: " + tabla;
					errores.Add(errorMsg);

					Bitacora.RegistrarErrorDVH(tabla, userName);
				}
				if(verificarDVH(tabla, userName)) {
                    ok = false;
                }
			}

			return ok;
		}

		public static string obtenerErrores() {
			var result = new System.Text.StringBuilder();
			foreach (string error in errores) {
				result.Append(error + "</br>");
			}

			return result.ToString();
		}

		public static void actualizarDV() {
			foreach(string tabla in tablas) {
				DVDAO.actualizarDVH(tabla);
				DVDAO.actualizarDVV(tabla);
			}
		}

		public static void borrarErrores() {
			errores.Clear();
		}
	}
}
