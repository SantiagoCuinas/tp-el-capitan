﻿function checkString(data) {
	data = data.trim();
	return data === null || data === "";
}

function urlParam(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results === null) {
		return null;
	}
	return decodeURI(results[1]) || 0;
}