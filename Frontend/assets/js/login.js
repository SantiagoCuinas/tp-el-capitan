﻿$(document).ready(function () {

	$("#form-login").submit(function (e) {
		e.preventDefault;
		var values = $(this).serializeArray();

		if (checkString(values[0].value) || checkString(values[1].value)) {
			var errorMsg = $('#errorMsg');
			errorMsg.text("Complete los campos");
			errorMsg.show();
			return false;
		}

		return true;
	});

	if (urlParam('login') === 'failed') {
		var errorMsg = $('#errorMsg');
		errorMsg.text("El usuario y contraseña no coinciden.");
		errorMsg.show();
	}
});