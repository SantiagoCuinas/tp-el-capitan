﻿$(document).ready(function () {

	if (urlParam('backup') === 'ok') {
		var successMsg = $('#successMsg');
		successMsg.text("El backup fue realizado con exito.");
		successMsg.show();
	}

	if (urlParam('restore') === 'ok') {
		var successMsg = $('#successMsg');
		successMsg.text("El restore fue realizado con exito.");
		successMsg.show();
	}
});