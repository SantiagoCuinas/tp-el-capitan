﻿using SL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page {
	protected void Page_Load(object sender, EventArgs e) {
		if (Convert.ToInt32(Session["role"]) != 3) {
			String User;
			if (Convert.ToString(Session["username"]) == "") { User = "Guest"; } else { User = Convert.ToString(Session["username"]); }
			SL.Bitacora.RegistrarIngresoBitacoraUrl(User);
			Response.Redirect("/Home.aspx");
		}
	}
}