﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-5 offset-md-1">
					<form id="form-login" class="w-100 text-left" method="POST" action="loginAction.aspx">
						<h1>Iniciar sesion</h1>
						<div id="errorMsg" class="alert alert-danger" role="alert"></div>
						<div class="form-group">
							<label for="username">Nombre de usuario</label>
							<input type="text" class="form-control" name="username" placeholder="Nombre de Usuario">
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="password" placeholder="Password">
						</div>
						<input type="submit" class="btn btn-primary" value="Iniciar" />
					</form>
				</div>
			</div>
		</div>
	</div>

	<script src="assets/js/login.js"></script>
</asp:Content>
