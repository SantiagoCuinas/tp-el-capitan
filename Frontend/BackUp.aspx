﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BackUp.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<form id="form1" runat="server" class="m-0">
	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-7">
					<div class="main-dialog-scroll">
						<h1>Back Up</h1>
						<div id="successMsg" class="alert alert-success" role="alert"></div>
						<asp:Button ID="Button1" class="btn btn-primary" runat="server" OnClick="Button1_Click" Text="BackUp" />
						<asp:Button ID="Button2" class="btn btn-secondary" runat="server" OnClick="Button2_Click" Text="Restore" />
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>

	<script src="assets/js/backup.js"></script>

</asp:Content>

