﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Bitacora.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<link href="/Bitacora.css" rel="stylesheet">
	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-7">
					<div class="main-dialog-scroll">
						<h1>Bitácora</h1>
						<%
							if (Convert.ToInt32(Session["role"]) != 3) {
								String User;
								if (Convert.ToString(Session["username"]) == "") { User = "Guest"; } else { User = Convert.ToString(Session["username"]); }
								SL.Bitacora.RegistrarIngresoBitacoraUrl(User);
								Response.Redirect("/Home.aspx");
							}
							%> 
							<table id="bitacoraData" style="width:100%">
								<tr>
									<th></th>
									<th>Fecha</th> 
									<th>Descripción</th>
								</tr>
							<%
								foreach (DTO.MensajeDTO mensaje in SL.Bitacora.GetMensajes()) {
									Response.Write("<tr> " +
										"<td> <i class='material-icons'>" + mensaje.tipo + "</i> </td>" +
										"<td> [" + mensaje.time + "] </td>" +
										"<td> (" + mensaje.usuario + ") : " + mensaje.desc + "</td>" +
									"</tr>");
								}
							%>
							</table>
					</div>
				</div>
			</div>
		</div>
	</div>



</asp:Content>

