﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="lupulos.aspx.cs" Inherits="lupulos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

	<div class="container">
		<div class="row m-3">
			<h1>Lúpulo</h1>
		</div>
		<div class="row">
			<div class="col-12 col-md-4">
				<div class="card" style="background-image: url('assets/img/malta.jpg')">
					<div class="card-text">
						<h3>Collumbus</h3>
						<i class="material-icons">add_shopping_cart</i>
						<p>$65.30</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="card" style="background-image: url('assets/img/levadura.jpg')">
					<div class="card-text">
						<h3>Zeus</h3>
						<i class="material-icons">add_shopping_cart</i>
						<p>$61.30</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="card" style="background-image: url('assets/img/lupulo.jpg')">
					<div class="card-text">
						<h3>Mosaic</h3>
						<i class="material-icons">add_shopping_cart</i>
						<p>$73.30</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="card" style="background-image: url('assets/img/kits.jpg')">
					<div class="card-text">
						<h3>Equinox</h3>
						<i class="material-icons">add_shopping_cart</i>
						<p>$48.30</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
