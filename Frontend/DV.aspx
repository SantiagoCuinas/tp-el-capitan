﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DV.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<form id="form1" runat="server" class="m-0">
	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-7">
					<div class="main-dialog-scroll">
						<h1>Digito Verificador</h1>
						<%
							if (Convert.ToInt32(Session["role"]) != 3) {
								String User;
								if (Convert.ToString(Session["username"]) == "") { User = "Guest"; } else { User = Convert.ToString(Session["username"]); }
								SL.Bitacora.RegistrarIngresoBitacoraUrl(User);
								Response.Redirect("/Home.aspx");
							}
							%> 

						<asp:Button ID="Button1" class="btn btn-secondary" runat="server" OnClick="Button1_Click" Text="Calcular DV" />
					</div>
				</div>
			</div>
		</div>
	</div>



	</form>



</asp:Content>

