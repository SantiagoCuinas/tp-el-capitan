﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LogTest : System.Web.UI.Page {
	protected void Page_Load(object sender, EventArgs e) {
		String Username = Request.Form["username"];
		String Pass = Request.Form["password"];

		DTO.UsuarioDTO User = SL.LogIn.Ingresar(Username, Pass);
		if (User != null) {
			SL.Bitacora.RegistrarLogin(Username);
			Session["role"] = User.rol;
			Session["username"] = Username;

			if(!SL.DV.verificarDV(Username)) {
				if (User.rol == 3) {
					Response.Redirect("/ErrorDV.aspx");
				}
			}

			Response.Redirect("/Welcome.aspx");
		} else {
			Response.Redirect("/Login.aspx?login=failed");
		}
	}
}