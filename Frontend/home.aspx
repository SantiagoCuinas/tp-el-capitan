﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-4 offset-md-2">
					<div class="main-dialog">
						<img src="assets/img/lupulo.png">
						<h1>EL CAPITÁN</h1>
						<p>
							Nuestro objetivo es acercar a la comunidad cervecera, los más variados insumos y materias primas
                            de mejor calidad, a precios competitivos y al alcance de todos.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="sub-section">
		<div class="container">
			<div class="row">
				<div class="sub-container full-screen col-12 col-md-6 offset-md-6">
					<div class="p-5">
						<p>
							Creemos que es importante fomentar la actividad artesanal de producir cerveza y el intercambio de
                            experiencias y nos encanta que cada veas sean más lo que se interesan en el creciente y apasionante
                            mundo de la elaboración casera.
						</p>
						<p>
							Te invitamos a visitar nuestra tienda online, donde encontrarás todos los insumos para dar tu "primer
                            paso" o el "gran salto".
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

</asp:Content>

