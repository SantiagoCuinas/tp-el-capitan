﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Welcome.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-6 offset-md-6">
					<div class="main-dialog">
						<%
							if (Convert.ToInt32(Session["role"]) == 3) {
								Response.Write("<h1>Usted ha ingresado como Web Master</h1><p>Tendra permitido acceder a las herramientas del administrador del sistema.");
							} else if (Convert.ToInt32(Session["role"]) == 2) {
								Response.Write("<h1>Usted ingresó con un usuario administrativo / opertativo</h1><p>Podra modificar los precios de los productos, su ubicacion y la información de los empleados.");
							} else if (Convert.ToInt32(Session["role"]) == 1) {
								Response.Write("<h1>Usted ingresó con un usuario cliente</h1><p>Tendra acceso al carrito de compras</p>");
							} else {
								Response.Write("<h1>No se ha ingresado con ningun usuario</h1><p>Por favor, inicie sesión.</p");
							}

						%>
					</div>
				</div>
			</div>
		</div>
	</div>



</asp:Content>

