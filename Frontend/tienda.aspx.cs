﻿using DTO;
using System;
using System.Collections.Generic;
using WebService;

public partial class tienda : System.Web.UI.Page {
    public List<Producto> productos;

    protected void Page_Load(object sender, EventArgs e) {
		string filter = Request["filter"] == null? "" : Request["filter"];
		TiendaWS tiendaWs = new TiendaWS();
		productos = tiendaWs.obtenerProductos(filter);
	}


	protected void Button2_Click(object sender, EventArgs e) {

	}
}