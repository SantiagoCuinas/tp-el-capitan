﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="tienda.aspx.cs" Inherits="tienda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

	<form id="form1" runat="server">

<div class="container">
<div class="row mx-2">
<div class="my-3">
<h1>Tienda OnLine</h1>
</div>
<div class="m-2">
<p>
Te ofrecemos una completa línea de productos vinculados a la producción cervecera, abasteciendo desde Homebrewbeer
hasta microcervecerías de producción industrializada.
</p>

<p>
Buscá en nuestras secciones de insumos, productos nacionales e importados, para elaborar tu propia receta.
Entre el resto de los productos encontraras el equipamiento necesario para cada etapa de producción.
</p>

	<input id="Text1" type="text" class="mr-1" /><a id="filter-link" class="btn btn-primary">Filtrar</a>

</div>
</div>
<div class="row">
<% foreach(var prod in productos) { %>
<% if(true) { %>
<div class="col-sm-12 col-md-4">
<div class="card" style="background-image: url('<%=prod.imagen %>')">
	<div class="card-text">
		<h2><%=prod.nombre %></h2>
	</div>
</div>
	</div>
<% } %>
<% } %>
</div>
</div>

	</form>

	<script src="assets/js/tienda.js"></script>

</asp:Content>
