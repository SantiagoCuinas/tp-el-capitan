﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ErrorDV.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<form id="form1" runat="server" class="m-0">
	<div class="main-section">
		<div class="container">
			<div class="row">
				<div class="main-container full-screen col-12 col-md-7">
					<div class="main-dialog-scroll">
						<h1>Error</h1>
						<div class="alert alert-danger" role="alert">
							Ha ocurrido un error al verificar la consistencia de la base de datos.
							<p><% Response.Write(SL.DV.obtenerErrores()); %></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	</form>



</asp:Content>

