﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL {
	public class DVDAO {
		static DBAccess dbaa = new DBAccess();

		public static bool verificarDVV(string tabla) {
			string dvvCalculado = calcularDVV(tabla).GetHashCode().ToString();
			return dvvCalculado.Equals(obtenerDVV(tabla));
		}

		public static string calcularDVV(string tabla) {
			SqlConnection conn = dbaa.GetCon();
			string selectDVH = $"SELECT dvh FROM {tabla}";
			SqlCommand query = new SqlCommand(selectDVH, conn);
			conn.Open();
			SqlDataReader data = query.ExecuteReader();

			var dvhs = new System.Text.StringBuilder();
			while (data.Read()) {
				dvhs.Append(data["dvh"]);
			}
			conn.Close();

			return dvhs.ToString();
		}

		public static string obtenerDVV(string tabla) {
			SqlConnection conn = dbaa.GetCon();
			string selectDVV = $"SELECT dvv FROM dvv WHERE nombre_tabla = '{tabla}'";
			SqlCommand query = new SqlCommand(selectDVV, conn);
			conn.Open();
			SqlDataReader data = query.ExecuteReader();

			string dvv = "";
			while (data.Read()) {
				dvv = data["dvv"].ToString();
			}
			conn.Close();

			return dvv;
		}

		public static List<string> verificarDVH(string tabla) {
			SqlConnection conn = dbaa.GetCon();
			List<string> result = new List<string>();

			string selectDVH = $"SELECT * FROM {tabla}";
			SqlCommand query = new SqlCommand(selectDVH, conn);
			conn.Open();
			SqlDataReader data = query.ExecuteReader();

			var dvhs = new System.Text.StringBuilder();
			int row = 1;
			while (data.Read()) {
				for (int i = 0; i < data.FieldCount; i++) {
					if (!data.GetName(i).Equals("dvh") && !DBAccess.esForeignKey(data.GetName(i))) {
						dvhs.Append(data.GetValue(i).ToString());
					}
				}

				string dvhsCalculated = dvhs.ToString().GetHashCode().ToString();
				if (dvhsCalculated != data["dvh"].ToString()) {
					result.Add(row.ToString());
				}
				row++;
				dvhs.Clear();
			}
			conn.Close();

			return result;
		}

		public static void actualizarDVV(string tabla) {
			SqlConnection mCon = dbaa.GetCon();
			string SQL = $"UPDATE Dvv SET dvv = '{calcularDVV(tabla).GetHashCode().ToString()}' WHERE nombre_tabla = '{tabla}'";
			mCon.Open();
			SqlCommand mCom = new SqlCommand(SQL, mCon);

			mCom.ExecuteNonQuery();
			mCon.Close();
		}

		public static void actualizarDVH(string tabla) {
			SqlConnection mCon = dbaa.GetCon();
			List<string> result = new List<string>();

			string selectDVH = $"SELECT * FROM {tabla}";
			SqlCommand selectQuery = new SqlCommand(selectDVH, mCon);
			mCon.Open();
			SqlDataReader data = selectQuery.ExecuteReader();

			var finalQuery = new System.Text.StringBuilder();
			var dvhs = new System.Text.StringBuilder();
			int row = 1;
			while (data.Read()) {
				for (int i = 0; i < data.FieldCount; i++) {
					if (!data.GetName(i).Equals("dvh") && !DBAccess.esForeignKey(data.GetName(i))) {
						dvhs.Append(data.GetValue(i).ToString());
					}
				}

				finalQuery.Append($"UPDATE {tabla} SET dvh = '{dvhs.ToString().GetHashCode().ToString()}' WHERE id = {data["id"].ToString()};");
				row++;
				dvhs.Clear();
			}
			mCon.Close();

			mCon.Open();
			SqlCommand updateQuery = new SqlCommand(finalQuery.ToString(), mCon);
			updateQuery.ExecuteNonQuery();
			mCon.Close();
		}
	}
}
