﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class DBAccess {
	//Santi  "Data Source=DESKTOP-9JMU4GB\MSSQLSERVER2;Initial Catalog=ElCapitan;Integrated Security=True"
	//Martin  "Data Source=DESKTOP-HE4OMGN;Initial Catalog=ElCapitan;Integrated Security=True"


	SqlConnection conn = new SqlConnection(@"Data Source=localhost;Initial Catalog=ElCapitan;Integrated Security=True");

	public SqlConnection GetCon() {
		return conn;
	}

	public static bool esForeignKey(string colName) {
		if (colName.Length < 4) {
			return false;
		}

		if (colName.Substring(colName.Length - 3, 3).Equals("_id")) {
			return true;
		}

		return false;
	}
}
