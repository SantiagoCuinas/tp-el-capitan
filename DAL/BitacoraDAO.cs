﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL {
	public class BitacoraDAO {
		DBAccess dbaa = new DBAccess();
		SqlConnection mCon;

		public void Registrar(MensajeDTO pMensaje) {
			mCon = dbaa.GetCon();
			mCon.Open();
			string SQL = "INSERT INTO Bitacora (Fecha, Usuario, Descripcion, Tipo) values ('" + Convert.ToString(pMensaje.time) + "','" + pMensaje.usuario + "','" + pMensaje.desc + "','" + pMensaje.tipo + "')";
			SqlCommand mCom = new SqlCommand(SQL, mCon);
			mCom.ExecuteNonQuery();
			mCon.Close();
		}

		public List<MensajeDTO> GetMensajes() {

			mCon = dbaa.GetCon();
			mCon.Open();
			string SQL = "SELECT TOP (30) * FROM Bitacora order by Fecha DESC";
			SqlCommand mCom = new SqlCommand(SQL, mCon);
			SqlDataReader mReader = mCom.ExecuteReader();
			List<MensajeDTO> ListaMensajes = new List<MensajeDTO>();
			if (mReader.HasRows) {
				do {
					try {
						ListaMensajes.Add(new MensajeDTO(Convert.ToDateTime(mReader["Fecha"].ToString()), mReader["Usuario"].ToString(), mReader["Descripcion"].ToString(),mReader["Tipo"].ToString()));
					} catch {
					}
				} while (mReader.Read());
			}
			mReader.Close();
			mCon.Close();
			return ListaMensajes;
		}

	}
}
