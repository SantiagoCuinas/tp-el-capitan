﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL {
	public class BackupDAO {
		DBAccess dbaa = new DBAccess();

		public void backUp(string path) {
			SqlConnection mCon = dbaa.GetCon();

			SqlCommand query = new SqlCommand("BACKUP DATABASE ElCapitan TO  DISK = @bkpPath", mCon);
			string bkpPath = path + "\\..\\BackUps\\bkp.bak";
			File.Delete(bkpPath);
			query.Parameters.AddWithValue("@bkpPath", bkpPath);

			mCon.Open();
			query.ExecuteNonQuery();
			mCon.Close();
		}

		public void restore(string path) {
			SqlConnection mCon = dbaa.GetCon();

			SqlCommand singleUser = new SqlCommand("ALTER DATABASE ElCapitan SET Single_User WITH Rollback Immediate", mCon);
			SqlCommand query = new SqlCommand("USE master; RESTORE DATABASE ElCapitan FROM DISK = @bkpPath WITH REPLACE;", mCon);
			SqlCommand multiUser = new SqlCommand("ALTER DATABASE ElCapitan SET Multi_User", mCon);

			string bkpPath = path + "\\..\\BackUps\\bkp.bak";
			query.Parameters.AddWithValue("@bkpPath", bkpPath);

			mCon.Open();
			singleUser.ExecuteNonQuery();
			query.ExecuteNonQuery();
			multiUser.ExecuteNonQuery();
			mCon.Close();
		}
	}
}
