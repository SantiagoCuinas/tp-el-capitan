﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL {
	public class UsuariosDAO {
		DBAccess dbaa = new DBAccess();
		SqlConnection mCon;

		public String GetPass(String pUser) {
			mCon = dbaa.GetCon();
			mCon.Open();
			string SQL = "SELECT Pass from Usuarios where NombreUsuario = @pUser and Estado = 0";
			SqlCommand mCom = new SqlCommand(SQL, mCon);

			mCom.Parameters.Add("@pUser", SqlDbType.VarChar);
			mCom.Parameters["@pUser"].Value = pUser;

			SqlDataReader mReader = mCom.ExecuteReader();
			String pass = "";
			if (mReader.HasRows) {
				do {
					try {
						pass = mReader["Pass"].ToString();
					} catch {
					}
				} while (mReader.Read());
			}
			mReader.Close();
			mCon.Close();
			return pass;
		}

		public void CambiarEstado(string pUser) {
			mCon = dbaa.GetCon();
			mCon.Open();
			string SQL = "Update Usuarios set Estado = 1 where NombreUsuario = @pUser";
			SqlCommand mCom = new SqlCommand(SQL, mCon);

			mCom.Parameters.Add("@pUser", SqlDbType.VarChar);
			mCom.Parameters["@pUser"].Value = pUser;

			mCom.ExecuteNonQuery();
			mCon.Close();
		}

		public void CambiarNumeroIntentos(string pUser, int Intentos) {
			mCon = dbaa.GetCon();
			mCon.Open();
			string SQL = "Update Usuarios set Intentos = @Intentos where NombreUsuario = @pUser";
			SqlCommand mCom = new SqlCommand(SQL, mCon);

			mCom.Parameters.Add("@Intentos", SqlDbType.Int);
			mCom.Parameters["@Intentos"].Value = Intentos;
			mCom.Parameters.Add("@pUser", SqlDbType.VarChar);
			mCom.Parameters["@pUser"].Value = pUser;

			mCom.ExecuteNonQuery();
			mCon.Close();
		}



		public DTO.UsuarioDTO GetUsuario(string pUser) {
			mCon = dbaa.GetCon();
			mCon.Open();
			string SQL = "SELECT * from Usuarios inner join Permisos on Usuarios.Id = Permisos.IdUsuario where NombreUsuario = @pUser and Estado = 0";
			SqlCommand mCom = new SqlCommand(SQL, mCon);

			mCom.Parameters.Add("@pUser", SqlDbType.VarChar);
			mCom.Parameters["@pUser"].Value = pUser;

			SqlDataReader mReader = mCom.ExecuteReader();
			DTO.UsuarioDTO nuevo = new DTO.UsuarioDTO(0, "", "", 0, 4); ;
			if (mReader.HasRows) {
				do {
					try {
						nuevo = new DTO.UsuarioDTO(Convert.ToInt32(mReader["Id"]), mReader["NombreUsuario"].ToString(), mReader["Pass"].ToString(), Convert.ToInt32(mReader["NivelPermiso"]), Convert.ToInt32(mReader["Intentos"]));
					} catch { }
				} while (mReader.Read());
				mReader.Close();
				mCon.Close();
				return nuevo;
			} else {
				mReader.Close();
				mCon.Close();
				return null;
			}
		}




	}
}

