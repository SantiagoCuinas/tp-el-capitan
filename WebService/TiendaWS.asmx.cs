﻿using DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using System.Xml.XPath;

namespace WebService {
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TiendaWS : System.Web.Services.WebService {

        [WebMethod]
		public List<Producto> obtenerProductos(string filter = "") {
			List<Producto> productos = new List<Producto>();
			filter = filter.ToUpper();

			foreach (XElement level1Element in XElement.Load(Server.MapPath("assets/xml/Inventario.xml")).Elements("Producto")) {
				Producto p1 = new Producto();
				bool inFilter = true;

				foreach (XElement level2Element in level1Element.Elements("ProductoNombre")) {
					string value = level2Element.Value;
					if (filter != "" && !value.ToUpper().Split(' ').Contains(filter)) {
						inFilter = false;
					}
					p1.nombre = level2Element.Value;
				}
				foreach (XElement level2Element in level1Element.Elements("ProductoImagen")) {
					p1.imagen = level2Element.Value;
				}

				if (inFilter) {
					productos.Add(p1);
				}
			}

			return productos;
		}
	}
}
