USE [master]
GO
/****** Object:  Database [ElCapitan]    Script Date: 7/14/2019 16:50:26 ******/
CREATE DATABASE [ElCapitan]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ElCapitan', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ElCapitan.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ElCapitan_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ElCapitan_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ElCapitan] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ElCapitan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ElCapitan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ElCapitan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ElCapitan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ElCapitan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ElCapitan] SET ARITHABORT OFF 
GO
ALTER DATABASE [ElCapitan] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ElCapitan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ElCapitan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ElCapitan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ElCapitan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ElCapitan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ElCapitan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ElCapitan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ElCapitan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ElCapitan] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ElCapitan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ElCapitan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ElCapitan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ElCapitan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ElCapitan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ElCapitan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ElCapitan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ElCapitan] SET RECOVERY FULL 
GO
ALTER DATABASE [ElCapitan] SET  MULTI_USER 
GO
ALTER DATABASE [ElCapitan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ElCapitan] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ElCapitan] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ElCapitan] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ElCapitan] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ElCapitan', N'ON'
GO
ALTER DATABASE [ElCapitan] SET QUERY_STORE = OFF
GO
USE [ElCapitan]
GO
/****** Object:  Table [dbo].[Bitacora]    Script Date: 7/14/2019 16:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bitacora](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [varchar](50) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Tipo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Bitacora] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dvv]    Script Date: 7/14/2019 16:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dvv](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre_tabla] [varchar](50) NULL,
	[dvv] [varchar](50) NULL,
 CONSTRAINT [PK_Dvv_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisos]    Script Date: 7/14/2019 16:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[NivelPermiso] [int] NOT NULL,
	[dvh] [varchar](50) NULL,
 CONSTRAINT [PK_Permisos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 7/14/2019 16:50:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id] [int] NOT NULL,
	[NombreUsuario] [varchar](50) NOT NULL,
	[Pass] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[Intentos] [int] NOT NULL,
	[dvh] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bitacora] ON 

INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (1, N'30-May-18 16:44:07', N'admin', N'El usuario salió del sistema.', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (2, N'30-May-18 16:44:10', N'admin', N'El usuario ingresó al sistema.', N'error')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (3, N'30-May-18 16:48:12', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (4, N'30-May-18 16:54:50', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (5, N'01-Jun-18 20:23:20', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (6, N'01-Jun-18 20:34:12', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (7, N'01-Jun-18 20:35:33', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (8, N'01-Jun-18 20:40:44', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (9, N'01-Jun-18 20:42:54', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (10, N'01-Jun-18 20:50:24', N'admin', N'El usuario ingresó al sistema.', N'error')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (11, N'01-Jun-18 20:50:36', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (12, N'02-Jun-18 14:05:30', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (13, N'06-Jun-18 22:12:05', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (14, N'09-Jun-18 12:55:46', N'admin', N'El usuario ingresó al sistema.', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (15, N'11-Jun-18 20:09:44', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (16, N'25-Jun-18 0:03:27', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (17, N'25-Jun-18 0:11:10', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (18, N'25-Jun-18 0:15:50', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (19, N'25-Jun-18 0:18:18', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (20, N'25-Jun-18 0:38:40', N'admin', N'Se ha realizado un restore', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (21, N'25-Jun-18 0:39:19', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (22, N'25-Jun-18 1:02:49', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (23, N'25-Jun-18 1:02:53', N'admin', N'holis', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (24, N'25-Jun-18 1:03:22', N'admin', N'holis', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (25, N'25-Jun-18 1:03:27', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (26, N'25-Jun-18 1:03:55', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (27, N'25-Jun-18 1:04:29', N'admin', N'Se ha realizado un restore', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (28, N'25-Jun-18 10:24:47', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (29, N'25-Jun-18 11:37:22', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (30, N'25-Jun-18 11:40:43', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (31, N'25-Jun-18 11:41:27', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (32, N'25-Jun-18 11:42:38', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (33, N'25-Jun-18 11:45:44', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (34, N'25-Jun-18 11:47:20', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (35, N'25-Jun-18 11:49:27', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (36, N'25-Jun-18 11:50:56', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (37, N'25-Jun-18 11:54:27', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (38, N'25-Jun-18 11:54:31', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (39, N'25-Jun-18 11:54:37', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (40, N'25-Jun-18 13:50:02', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (41, N'25-Jun-18 13:50:09', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (42, N'25-Jun-18 13:50:42', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (43, N'25-Jun-18 13:51:17', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (44, N'25-Jun-18 13:51:31', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (45, N'25-Jun-18 13:52:16', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (46, N'26-Jun-18 22:22:06', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (47, N'04-Jul-18 16:46:17', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (48, N'04-Jul-18 17:42:51', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (49, N'04-Jul-18 17:42:56', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (50, N'05-Jul-18 19:55:52', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (51, N'05-Jul-18 19:56:01', N'admin', N'Se ha realizado un restore', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (52, N'05-Jul-18 19:59:12', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (53, N'05-Jul-18 19:59:12', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (54, N'05-Jul-18 19:59:13', N'Guest', N'Se ha intentado ingresar a la bitácora por la URL.', N'error')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (55, N'05-Jul-18 19:59:33', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (56, N'05-Jul-18 19:59:33', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (57, N'05-Jul-18 19:59:33', N'Guest', N'Se ha intentado ingresar a la bitácora por la URL.', N'error')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (58, N'05-Jul-18 19:59:52', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (59, N'05-Jul-18 19:59:52', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (60, N'05-Jul-18 19:59:52', N'Guest', N'Se ha intentado ingresar a la bitácora por la URL.', N'error')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (61, N'05-Jul-18 20:00:41', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (62, N'05-Jul-18 20:00:41', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (63, N'05-Jul-18 20:04:06', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (64, N'05-Jul-18 20:04:06', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (65, N'05-Jul-18 20:04:06', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (66, N'05-Jul-18 20:04:11', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (67, N'05-Jul-18 20:05:07', N'MSobrero', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (68, N'05-Jul-18 20:05:07', N'MSobrero', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (69, N'05-Jul-18 20:05:07', N'MSobrero', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (70, N'05-Jul-18 20:05:20', N'MSobrero', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (71, N'05-Jul-18 20:06:00', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (72, N'05-Jul-18 20:06:00', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (73, N'05-Jul-18 20:06:00', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (74, N'05-Jul-18 20:06:17', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (75, N'05-Jul-18 20:06:20', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (76, N'05-Jul-18 20:06:30', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (77, N'05-Jul-18 20:07:41', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (78, N'05-Jul-18 20:07:44', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (79, N'05-Jul-18 20:07:44', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (80, N'05-Jul-18 20:07:44', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (81, N'05-Jul-18 20:07:44', N'admin', N'Error DVH en fila: 5', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (82, N'05-Jul-18 20:10:10', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (83, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (84, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (85, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 5', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (86, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (87, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (88, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 2', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (89, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (90, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (91, N'05-Jul-18 20:10:10', N'admin', N'Error DVH en fila: 5', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (92, N'05-Jul-18 20:14:27', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (93, N'05-Jul-18 20:14:27', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (94, N'05-Jul-18 20:14:38', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (95, N'05-Jul-18 20:16:30', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (96, N'05-Jul-18 20:16:31', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (97, N'05-Jul-18 20:17:38', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (98, N'05-Jul-18 20:17:38', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (99, N'05-Jul-18 20:18:10', N'admin', N'El usuario ingresó al sistema.', N'info')
GO
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (100, N'05-Jul-18 20:18:10', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (101, N'05-Jul-18 20:18:36', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (102, N'05-Jul-18 20:18:39', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (103, N'05-Jul-18 20:18:39', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (104, N'05-Jul-18 20:19:31', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (105, N'05-Jul-18 20:19:31', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (106, N'05-Jul-18 20:19:31', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (107, N'05-Jul-18 20:19:59', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (108, N'05-Jul-18 20:20:01', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (109, N'05-Jul-18 20:20:01', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (110, N'05-Jul-18 20:20:01', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (111, N'05-Jul-18 20:20:33', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (112, N'06-Jul-18 21:25:47', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (113, N'06-Jul-18 21:25:47', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (114, N'06-Jul-18 21:25:47', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (115, N'06-Jul-18 21:26:26', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (116, N'06-Jul-18 22:16:23', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (117, N'06-Jul-18 22:16:31', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (118, N'06-Jul-18 22:16:44', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (119, N'06-Jul-18 22:16:44', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (120, N'06-Jul-18 22:16:44', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (121, N'06-Jul-18 22:17:15', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (122, N'06-Jul-18 22:17:16', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (123, N'06-Jul-18 22:17:16', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (124, N'06-Jul-18 22:17:16', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (125, N'06-Jul-18 22:17:38', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (126, N'06-Jul-18 22:21:13', N'Guest', N'Se ha intentado ingresar a la bitácora por la URL.', N'error')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (127, N'06-Jul-18 22:21:16', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (128, N'06-Jul-18 22:21:16', N'admin', N'Error DVH en fila: Usuarios', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (129, N'06-Jul-18 22:21:16', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (130, N'06-Jul-18 22:21:33', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (131, N'06-Jul-18 22:21:35', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (132, N'06-Jul-18 22:21:39', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (133, N'06-Jul-18 22:40:25', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (134, N'06-Jul-18 22:40:53', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (135, N'06-Jul-18 22:42:50', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (136, N'06-Jul-18 22:43:04', N'admin', N'Se ha realizado un restore', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (137, N'06-Jul-18 22:43:55', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (138, N'06-Jul-18 22:54:19', N'admin', N'Se ha realizado un restore', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (139, N'06-Jul-18 22:54:38', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (140, N'06-Jul-18 22:54:42', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (141, N'06-Jul-18 22:54:44', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (142, N'12-Jul-18 19:45:08', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (143, N'12-Jul-18 19:45:25', N'admin', N'Se ha realizado un backup', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (144, N'18-Jul-18 14:18:00', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (145, N'03-Nov-18 16:35:10', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (146, N'04-Nov-18 22:31:29', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (147, N'04-Nov-18 22:31:42', N'admin', N'Se ha realizado un restore', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (148, N'04-Nov-18 22:32:01', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (149, N'04-Nov-18 22:34:55', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (150, N'04-Nov-18 22:37:01', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (151, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (152, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 2', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (153, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (154, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (155, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 5', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (156, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (157, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 2', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (158, N'04-Nov-18 22:37:01', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (159, N'04-Nov-18 22:37:02', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (160, N'04-Nov-18 22:37:02', N'admin', N'Error DVH en fila: 5', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (161, N'04-Nov-18 22:37:33', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (162, N'04-Nov-18 22:37:35', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (163, N'04-Nov-18 22:37:35', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (164, N'04-Nov-18 22:39:23', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (165, N'04-Nov-18 22:39:23', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (166, N'04-Nov-18 22:39:23', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (167, N'04-Nov-18 22:39:50', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (168, N'04-Nov-18 22:39:52', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (169, N'04-Nov-18 22:39:52', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (170, N'04-Nov-18 22:39:52', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (171, N'04-Nov-18 22:39:52', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (172, N'04-Nov-18 22:39:52', N'admin', N'Error DVH en fila: 2', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (173, N'04-Nov-18 22:41:50', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (174, N'04-Nov-18 22:41:50', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (175, N'04-Nov-18 22:41:50', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (176, N'04-Nov-18 22:41:50', N'admin', N'Error DVH en fila: Permisos', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (177, N'04-Nov-18 22:41:50', N'admin', N'Error DVH en fila: 2', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (178, N'04-Nov-18 22:44:23', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (179, N'04-Nov-18 22:44:37', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (180, N'04-Nov-18 22:44:38', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (181, N'04-Nov-18 22:44:38', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (182, N'04-Nov-18 22:44:49', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (183, N'04-Nov-18 22:45:09', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (184, N'04-Nov-18 22:45:09', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (185, N'04-Nov-18 22:45:30', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (186, N'04-Nov-18 22:45:31', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (187, N'04-Nov-18 22:45:31', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (188, N'04-Nov-18 22:45:41', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (189, N'04-Nov-18 22:45:43', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (190, N'04-Nov-18 22:46:06', N'admin', N'El usuario salió del sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (191, N'04-Nov-18 22:46:21', N'admin', N'El usuario ingresó al sistema.', N'info')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (192, N'04-Nov-18 22:46:21', N'admin', N'Error DVH en fila: 1', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (193, N'04-Nov-18 22:46:21', N'admin', N'Error DVH en fila: 3', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (194, N'04-Nov-18 22:46:21', N'admin', N'Error DVH en fila: 4', N'warning')
INSERT [dbo].[Bitacora] ([id], [Fecha], [Usuario], [Descripcion], [Tipo]) VALUES (195, N'04-Nov-18 22:46:21', N'admin', N'Error DVH en fila: 5', N'warning')
SET IDENTITY_INSERT [dbo].[Bitacora] OFF
SET IDENTITY_INSERT [dbo].[Dvv] ON 

INSERT [dbo].[Dvv] ([id], [nombre_tabla], [dvv]) VALUES (1, N'Usuarios', N'-2098020005')
INSERT [dbo].[Dvv] ([id], [nombre_tabla], [dvv]) VALUES (2, N'Permisos', N'-1567119029')
SET IDENTITY_INSERT [dbo].[Dvv] OFF
SET IDENTITY_INSERT [dbo].[Permisos] ON 

INSERT [dbo].[Permisos] ([id], [IdUsuario], [NivelPermiso], [dvh]) VALUES (1, 1, 2, N'-57589665')
INSERT [dbo].[Permisos] ([id], [IdUsuario], [NivelPermiso], [dvh]) VALUES (2, 2, 3, N'-1623739143')
INSERT [dbo].[Permisos] ([id], [IdUsuario], [NivelPermiso], [dvh]) VALUES (3, 3, 2, N'-57720739')
INSERT [dbo].[Permisos] ([id], [IdUsuario], [NivelPermiso], [dvh]) VALUES (4, 4, 1, N'1508297665')
INSERT [dbo].[Permisos] ([id], [IdUsuario], [NivelPermiso], [dvh]) VALUES (5, 5, 1, N'1508232128')
SET IDENTITY_INSERT [dbo].[Permisos] OFF
INSERT [dbo].[Usuarios] ([id], [NombreUsuario], [Pass], [Estado], [Intentos], [dvh]) VALUES (1, N'SCuinas', N'19a2854144b63a8f7617a6f225019b12', 0, 0, N'474408717')
INSERT [dbo].[Usuarios] ([id], [NombreUsuario], [Pass], [Estado], [Intentos], [dvh]) VALUES (2, N'admin', N'19a2854144b63a8f7617a6f225019b12', 0, 0, N'23758057')
INSERT [dbo].[Usuarios] ([id], [NombreUsuario], [Pass], [Estado], [Intentos], [dvh]) VALUES (3, N'MSobrero', N'19a2854144b63a8f7617a6f225019b12', 1, 0, N'1223556535')
INSERT [dbo].[Usuarios] ([id], [NombreUsuario], [Pass], [Estado], [Intentos], [dvh]) VALUES (4, N'EVega', N'19a2854144b63a8f7617a6f225019b12', 1, 3, N'492183799')
INSERT [dbo].[Usuarios] ([id], [NombreUsuario], [Pass], [Estado], [Intentos], [dvh]) VALUES (5, N'JDiaz', N'19a2854144b63a8f7617a6f225019b12', 0, 0, N'-1300570479')
USE [master]
GO
ALTER DATABASE [ElCapitan] SET  READ_WRITE 
GO
