﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO {
	public class UsuarioDTO {
		public int id { get; set; }
		public string pass { get; set; }
		public string username { get; set; }
		public int rol { get; set; }
		public int intentos { get; set; }

		public UsuarioDTO(int pId, string pName, String pPass, int pRol, int pIntentos) {
			this.id = pId;
			this.username = pName;
			this.pass = pPass;
			this.rol = pRol;
			this.intentos = pIntentos;
		}

		public bool isAdmin() {
			if (this.rol == 3) { return true; } else { return false; }
		}
	}
}
