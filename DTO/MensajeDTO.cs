﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO {
	public class MensajeDTO {
		public DateTime time;
		public string usuario;
		public string desc;
		public string tipo;

		public MensajeDTO(DateTime pTime, string pUsuario, string pDesc, string pTipo) {
			this.time = pTime;
			this.usuario = pUsuario;
			this.desc = pDesc;
			this.tipo = pTipo;
		}
	}
}
