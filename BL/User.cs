﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class User {
	public int id { get; set; }
	public string fullname { get; set; }
	public string username { get; set; }
	public string rol { get; set; }

	public User() {
		id = 1;
		username = "uname";
		fullname = "fname";
		rol = "admin";
	}

	public bool isAdmin() {
		return true;
	}
}
